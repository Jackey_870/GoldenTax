﻿namespace GoldenTax
{
    partial class FormExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.FormSpliter = new System.Windows.Forms.SplitContainer();
        	this.checkBoxDate = new System.Windows.Forms.CheckBox();
        	this.labelTo = new System.Windows.Forms.Label();
        	this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
        	this.dateTimePickerBegin = new System.Windows.Forms.DateTimePicker();
        	this.buttonExport = new System.Windows.Forms.Button();
        	this.buttonQuery = new System.Windows.Forms.Button();
        	this.textBoxCondition = new System.Windows.Forms.TextBox();
        	this.labelCondition = new System.Windows.Forms.Label();
        	this.comboBoxFiltField = new System.Windows.Forms.ComboBox();
        	this.labelFilt = new System.Windows.Forms.Label();
        	this.dataGridViewInvoice = new System.Windows.Forms.DataGridView();
        	this.FDetailId = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.红蓝字 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	((System.ComponentModel.ISupportInitialize)(this.FormSpliter)).BeginInit();
        	this.FormSpliter.Panel1.SuspendLayout();
        	this.FormSpliter.Panel2.SuspendLayout();
        	this.FormSpliter.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridViewInvoice)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// FormSpliter
        	// 
        	this.FormSpliter.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.FormSpliter.IsSplitterFixed = true;
        	this.FormSpliter.Location = new System.Drawing.Point(0, 0);
        	this.FormSpliter.Name = "FormSpliter";
        	this.FormSpliter.Orientation = System.Windows.Forms.Orientation.Horizontal;
        	// 
        	// FormSpliter.Panel1
        	// 
        	this.FormSpliter.Panel1.Controls.Add(this.checkBoxDate);
        	this.FormSpliter.Panel1.Controls.Add(this.labelTo);
        	this.FormSpliter.Panel1.Controls.Add(this.dateTimePickerEnd);
        	this.FormSpliter.Panel1.Controls.Add(this.dateTimePickerBegin);
        	this.FormSpliter.Panel1.Controls.Add(this.buttonExport);
        	this.FormSpliter.Panel1.Controls.Add(this.buttonQuery);
        	this.FormSpliter.Panel1.Controls.Add(this.textBoxCondition);
        	this.FormSpliter.Panel1.Controls.Add(this.labelCondition);
        	this.FormSpliter.Panel1.Controls.Add(this.comboBoxFiltField);
        	this.FormSpliter.Panel1.Controls.Add(this.labelFilt);
        	// 
        	// FormSpliter.Panel2
        	// 
        	this.FormSpliter.Panel2.Controls.Add(this.dataGridViewInvoice);
        	this.FormSpliter.Size = new System.Drawing.Size(784, 562);
        	this.FormSpliter.SplitterDistance = 87;
        	this.FormSpliter.TabIndex = 0;
        	// 
        	// checkBoxDate
        	// 
        	this.checkBoxDate.AutoSize = true;
        	this.checkBoxDate.Checked = true;
        	this.checkBoxDate.CheckState = System.Windows.Forms.CheckState.Checked;
        	this.checkBoxDate.Location = new System.Drawing.Point(21, 24);
        	this.checkBoxDate.Name = "checkBoxDate";
        	this.checkBoxDate.Size = new System.Drawing.Size(72, 16);
        	this.checkBoxDate.TabIndex = 10;
        	this.checkBoxDate.Text = "日期范围";
        	this.checkBoxDate.UseVisualStyleBackColor = true;
        	// 
        	// labelTo
        	// 
        	this.labelTo.AutoSize = true;
        	this.labelTo.Location = new System.Drawing.Point(267, 25);
        	this.labelTo.Name = "labelTo";
        	this.labelTo.Size = new System.Drawing.Size(17, 12);
        	this.labelTo.TabIndex = 13;
        	this.labelTo.Text = "至";
        	// 
        	// dateTimePickerEnd
        	// 
        	this.dateTimePickerEnd.Location = new System.Drawing.Point(322, 16);
        	this.dateTimePickerEnd.Name = "dateTimePickerEnd";
        	this.dateTimePickerEnd.Size = new System.Drawing.Size(121, 21);
        	this.dateTimePickerEnd.TabIndex = 2;
        	this.dateTimePickerEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateTimePickerEndKeyDown);
        	// 
        	// dateTimePickerBegin
        	// 
        	this.dateTimePickerBegin.Location = new System.Drawing.Point(116, 19);
        	this.dateTimePickerBegin.Name = "dateTimePickerBegin";
        	this.dateTimePickerBegin.Size = new System.Drawing.Size(121, 21);
        	this.dateTimePickerBegin.TabIndex = 1;
        	this.dateTimePickerBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateTimePickerBeginKeyDown);
        	// 
        	// buttonExport
        	// 
        	this.buttonExport.Location = new System.Drawing.Point(678, 43);
        	this.buttonExport.Name = "buttonExport";
        	this.buttonExport.Size = new System.Drawing.Size(75, 23);
        	this.buttonExport.TabIndex = 6;
        	this.buttonExport.Text = "导出";
        	this.buttonExport.UseVisualStyleBackColor = true;
        	this.buttonExport.Click += new System.EventHandler(this.ButtonExportClick);
        	// 
        	// buttonQuery
        	// 
        	this.buttonQuery.Location = new System.Drawing.Point(562, 43);
        	this.buttonQuery.Name = "buttonQuery";
        	this.buttonQuery.Size = new System.Drawing.Size(75, 23);
        	this.buttonQuery.TabIndex = 5;
        	this.buttonQuery.Text = "查询";
        	this.buttonQuery.UseVisualStyleBackColor = true;
        	this.buttonQuery.Click += new System.EventHandler(this.ButtonQueryClick);
        	// 
        	// textBoxCondition
        	// 
        	this.textBoxCondition.Location = new System.Drawing.Point(322, 45);
        	this.textBoxCondition.Name = "textBoxCondition";
        	this.textBoxCondition.Size = new System.Drawing.Size(161, 21);
        	this.textBoxCondition.TabIndex = 4;
        	this.textBoxCondition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxConditionKeyDown);
        	// 
        	// labelCondition
        	// 
        	this.labelCondition.AutoSize = true;
        	this.labelCondition.Location = new System.Drawing.Point(252, 54);
        	this.labelCondition.Name = "labelCondition";
        	this.labelCondition.Size = new System.Drawing.Size(53, 12);
        	this.labelCondition.TabIndex = 2;
        	this.labelCondition.Text = "查询条件";
        	// 
        	// comboBoxFiltField
        	// 
        	this.comboBoxFiltField.BackColor = System.Drawing.Color.DarkGray;
        	this.comboBoxFiltField.FormattingEnabled = true;
        	this.comboBoxFiltField.Items.AddRange(new object[] {
			"无",
			"发票号码",
			"客户名称"});
        	this.comboBoxFiltField.Location = new System.Drawing.Point(116, 46);
        	this.comboBoxFiltField.Name = "comboBoxFiltField";
        	this.comboBoxFiltField.Size = new System.Drawing.Size(121, 20);
        	this.comboBoxFiltField.TabIndex = 3;
        	this.comboBoxFiltField.SelectedIndexChanged += new System.EventHandler(this.ComboBoxFiltFieldSelectedIndexChanged);
        	this.comboBoxFiltField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxFiltFieldKeyDown);
        	// 
        	// labelFilt
        	// 
        	this.labelFilt.AutoSize = true;
        	this.labelFilt.Location = new System.Drawing.Point(40, 54);
        	this.labelFilt.Name = "labelFilt";
        	this.labelFilt.Size = new System.Drawing.Size(53, 12);
        	this.labelFilt.TabIndex = 0;
        	this.labelFilt.Text = "查询参数";
        	// 
        	// dataGridViewInvoice
        	// 
        	this.dataGridViewInvoice.AllowUserToAddRows = false;
        	this.dataGridViewInvoice.AllowUserToDeleteRows = false;
        	this.dataGridViewInvoice.AllowUserToOrderColumns = true;
        	this.dataGridViewInvoice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
        	this.dataGridViewInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        	this.dataGridViewInvoice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.FDetailId,
			this.红蓝字,
			this.Column1,
			this.Column2,
			this.Column3,
			this.Column4,
			this.Column5,
			this.Column6,
			this.Column7,
			this.Column8,
			this.Column9,
			this.Column10,
			this.Column11,
			this.Column12,
			this.Column13,
			this.Column14,
			this.Column15,
			this.Column16,
			this.Column17,
			this.Column18,
			this.Column19,
			this.Column20,
			this.Column21,
			this.Column22,
			this.Column23,
			this.Column24,
			this.Column25,
			this.Column26,
			this.Column27});
        	this.dataGridViewInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dataGridViewInvoice.Location = new System.Drawing.Point(0, 0);
        	this.dataGridViewInvoice.Name = "dataGridViewInvoice";
        	this.dataGridViewInvoice.ReadOnly = true;
        	this.dataGridViewInvoice.RowTemplate.Height = 23;
        	this.dataGridViewInvoice.Size = new System.Drawing.Size(784, 471);
        	this.dataGridViewInvoice.TabIndex = 0;
        	// 
        	// FDetailId
        	// 
        	this.FDetailId.DataPropertyName = "FDetailId";
        	this.FDetailId.HeaderText = "ID";
        	this.FDetailId.Name = "FDetailId";
        	this.FDetailId.ReadOnly = true;
        	this.FDetailId.Visible = false;
        	this.FDetailId.Width = 42;
        	// 
        	// 红蓝字
        	// 
        	this.红蓝字.DataPropertyName = "Frob";
        	this.红蓝字.HeaderText = "红蓝字";
        	this.红蓝字.Name = "红蓝字";
        	this.红蓝字.ReadOnly = true;
        	this.红蓝字.Width = 66;
        	// 
        	// Column1
        	// 
        	this.Column1.DataPropertyName = "FInvoiceNumber";
        	this.Column1.HeaderText = "单据号码";
        	this.Column1.Name = "Column1";
        	this.Column1.ReadOnly = true;
        	this.Column1.Width = 78;
        	// 
        	// Column2
        	// 
        	this.Column2.DataPropertyName = "FCustName";
        	this.Column2.HeaderText = "购方名称";
        	this.Column2.Name = "Column2";
        	this.Column2.ReadOnly = true;
        	this.Column2.Width = 78;
        	// 
        	// Column3
        	// 
        	this.Column3.DataPropertyName = "FDuty";
        	this.Column3.HeaderText = "购方税号";
        	this.Column3.Name = "Column3";
        	this.Column3.ReadOnly = true;
        	this.Column3.Width = 78;
        	// 
        	// Column4
        	// 
        	this.Column4.DataPropertyName = "FAddres";
        	this.Column4.HeaderText = "购方地址电话";
        	this.Column4.Name = "Column4";
        	this.Column4.ReadOnly = true;
        	this.Column4.Width = 102;
        	// 
        	// Column5
        	// 
        	this.Column5.DataPropertyName = "FBank";
        	this.Column5.HeaderText = "购方银行账号";
        	this.Column5.Name = "Column5";
        	this.Column5.ReadOnly = true;
        	this.Column5.Width = 102;
        	// 
        	// Column6
        	// 
        	this.Column6.DataPropertyName = "FNotes";
        	this.Column6.HeaderText = "备注";
        	this.Column6.Name = "Column6";
        	this.Column6.ReadOnly = true;
        	this.Column6.Width = 54;
        	// 
        	// Column7
        	// 
        	this.Column7.DataPropertyName = "FChecker";
        	this.Column7.HeaderText = "复核人";
        	this.Column7.Name = "Column7";
        	this.Column7.ReadOnly = true;
        	this.Column7.Width = 66;
        	// 
        	// Column8
        	// 
        	this.Column8.DataPropertyName = "FReceiver";
        	this.Column8.HeaderText = "收款人";
        	this.Column8.Name = "Column8";
        	this.Column8.ReadOnly = true;
        	this.Column8.Width = 66;
        	// 
        	// Column9
        	// 
        	this.Column9.DataPropertyName = "FProductName";
        	this.Column9.HeaderText = "清单商品名称";
        	this.Column9.Name = "Column9";
        	this.Column9.ReadOnly = true;
        	this.Column9.Width = 102;
        	// 
        	// Column10
        	// 
        	this.Column10.DataPropertyName = "FDate";
        	this.Column10.HeaderText = "单据日期";
        	this.Column10.Name = "Column10";
        	this.Column10.ReadOnly = true;
        	this.Column10.Width = 78;
        	// 
        	// Column11
        	// 
        	this.Column11.DataPropertyName = "FComBank";
        	this.Column11.HeaderText = "销方银行账号";
        	this.Column11.Name = "Column11";
        	this.Column11.ReadOnly = true;
        	this.Column11.Width = 102;
        	// 
        	// Column12
        	// 
        	this.Column12.DataPropertyName = "FComAddress";
        	this.Column12.HeaderText = "销方地址电话";
        	this.Column12.Name = "Column12";
        	this.Column12.ReadOnly = true;
        	this.Column12.Width = 102;
        	// 
        	// Column13
        	// 
        	this.Column13.DataPropertyName = "IdTag";
        	this.Column13.HeaderText = "身份证校验标志";
        	this.Column13.Name = "Column13";
        	this.Column13.ReadOnly = true;
        	this.Column13.Width = 114;
        	// 
        	// Column14
        	// 
        	this.Column14.DataPropertyName = "SeaOilTag";
        	this.Column14.HeaderText = "海洋石油标志";
        	this.Column14.Name = "Column14";
        	this.Column14.ReadOnly = true;
        	this.Column14.Width = 102;
        	// 
        	// Column15
        	// 
        	this.Column15.DataPropertyName = "FItemName";
        	this.Column15.HeaderText = "货物名称";
        	this.Column15.Name = "Column15";
        	this.Column15.ReadOnly = true;
        	this.Column15.Width = 78;
        	// 
        	// Column16
        	// 
        	this.Column16.DataPropertyName = "FUnitName";
        	this.Column16.HeaderText = "计量单位";
        	this.Column16.Name = "Column16";
        	this.Column16.ReadOnly = true;
        	this.Column16.Width = 78;
        	// 
        	// Column17
        	// 
        	this.Column17.DataPropertyName = "FModel";
        	this.Column17.HeaderText = "规格型号";
        	this.Column17.Name = "Column17";
        	this.Column17.ReadOnly = true;
        	this.Column17.Width = 78;
        	// 
        	// Column18
        	// 
        	this.Column18.DataPropertyName = "FQty";
        	this.Column18.HeaderText = "数量";
        	this.Column18.Name = "Column18";
        	this.Column18.ReadOnly = true;
        	this.Column18.Width = 54;
        	// 
        	// Column19
        	// 
        	this.Column19.DataPropertyName = "FAmount";
        	this.Column19.HeaderText = "金额";
        	this.Column19.Name = "Column19";
        	this.Column19.ReadOnly = true;
        	this.Column19.Width = 54;
        	// 
        	// Column20
        	// 
        	this.Column20.DataPropertyName = "FTaxRate";
        	this.Column20.HeaderText = "税率";
        	this.Column20.Name = "Column20";
        	this.Column20.ReadOnly = true;
        	this.Column20.Width = 54;
        	// 
        	// Column21
        	// 
        	this.Column21.DataPropertyName = "TaxItems";
        	this.Column21.HeaderText = "商品税目";
        	this.Column21.Name = "Column21";
        	this.Column21.ReadOnly = true;
        	this.Column21.Width = 78;
        	// 
        	// Column22
        	// 
        	this.Column22.DataPropertyName = "FAmtDiscount";
        	this.Column22.HeaderText = "折扣金额";
        	this.Column22.Name = "Column22";
        	this.Column22.ReadOnly = true;
        	this.Column22.Width = 78;
        	// 
        	// Column23
        	// 
        	this.Column23.DataPropertyName = "FTaxAmount";
        	this.Column23.HeaderText = "税额";
        	this.Column23.Name = "Column23";
        	this.Column23.ReadOnly = true;
        	this.Column23.Width = 54;
        	// 
        	// Column24
        	// 
        	this.Column24.DataPropertyName = "FDiscountTaxAmount";
        	this.Column24.HeaderText = "折扣税额";
        	this.Column24.Name = "Column24";
        	this.Column24.ReadOnly = true;
        	this.Column24.Width = 78;
        	// 
        	// Column25
        	// 
        	this.Column25.DataPropertyName = "FDiscountRate";
        	this.Column25.HeaderText = "折扣率";
        	this.Column25.Name = "Column25";
        	this.Column25.ReadOnly = true;
        	this.Column25.Width = 66;
        	// 
        	// Column26
        	// 
        	this.Column26.DataPropertyName = "FPrice";
        	this.Column26.HeaderText = "单价";
        	this.Column26.Name = "Column26";
        	this.Column26.ReadOnly = true;
        	this.Column26.Width = 54;
        	// 
        	// Column27
        	// 
        	this.Column27.DataPropertyName = "PriceOffer";
        	this.Column27.HeaderText = "价格方式";
        	this.Column27.Name = "Column27";
        	this.Column27.ReadOnly = true;
        	this.Column27.Width = 78;
        	// 
        	// FormExport
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(784, 562);
        	this.Controls.Add(this.FormSpliter);
        	this.Name = "FormExport";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "金蝶发票导出";
        	this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        	this.FormSpliter.Panel1.ResumeLayout(false);
        	this.FormSpliter.Panel1.PerformLayout();
        	this.FormSpliter.Panel2.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.FormSpliter)).EndInit();
        	this.FormSpliter.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.dataGridViewInvoice)).EndInit();
        	this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer FormSpliter;
        private System.Windows.Forms.Label labelFilt;
        private System.Windows.Forms.ComboBox comboBoxFiltField;
        private System.Windows.Forms.Label labelCondition;
        private System.Windows.Forms.TextBox textBoxCondition;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.Button buttonQuery;
        private System.Windows.Forms.DataGridView dataGridViewInvoice;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.DateTimePicker dateTimePickerBegin;
        private System.Windows.Forms.DataGridViewTextBoxColumn FDetailId;
        private System.Windows.Forms.DataGridViewTextBoxColumn 红蓝字;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;

    }
}